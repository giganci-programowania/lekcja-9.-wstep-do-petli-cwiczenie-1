﻿using System;

namespace cwiczenie1
{
    class Program
    {
        // Napisać program konsolowy, który będzie wyświetlać wszystkie kolejne liczby podzielne przez 17 od 1 do 5000.
        // Wersja rozszerzona napisać program tak, aby nie korzystał z instrukcji if (o ile napisany program z niej korzysta).
        static void Main(string[] args)
        {
            Console.WriteLine("Program wyświetlający wszystkie liczby podzielne przez 17 mniejsze od 5000");
            for (int i = 0; i <= 5000; i += 17)
            {
                Console.WriteLine("Liczba podzielna przez 17: {0}", i);
            }
            Console.ReadKey();

        }
    }
}
